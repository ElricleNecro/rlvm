local bindings = {
	{ 'n', "<F1>", ":make!<cr>", { noremap = false, silent = false}},
	{ 'n', "<F2>", ":!premake5 gmake2<cr>", { noremap = false, silent = false}},
	{ 'n', "<F3>", ":!blasm examples/fibonacci.blasm -o /tmp/fib.bl", { noremap = false, silent = false}},
	{ 'n', "<F4>", ":!blvi /tmp/fib.bl", { noremap = false, silent = false}},
	{ 'n', "<F5>", ":!bear -- make<cr>", { noremap = false, silent = true}},
}

for _, bind in ipairs(bindings) do
	vim.api.nvim_set_keymap(bind[1], bind[2], bind[3], bind[4])
end

vim.o.makeprg = 'cargo'
vim.bo.makeprg = 'cargo'
vim.o.errorformat = table.concat(
	{
		"error\\[[A-Z0-9]+\\]: %m",
		"warning: %m",
		"--> %f:%l:%c",
	},
	','
)
