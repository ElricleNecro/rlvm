# RLVM: rust implementation of BLVM
[![pipeline status](https://gitlab.com/ElricleNecro/rlvm/badges/master/pipeline.svg)](https://gitlab.com/ElricleNecro/rlvm/-/commits/master)
[![coverage report](https://gitlab.com/ElricleNecro/rlvm/badges/master/coverage.svg)](https://gitlab.com/ElricleNecro/rlvm/-/commits/master)

see [BLVM](https://gitlab.com/ElricleNecro/blvm)
