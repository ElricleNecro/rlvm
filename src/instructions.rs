use std::convert::{From, TryFrom};
use std::fmt::{Display, Formatter};

use super::Word;

#[allow(non_camel_case_types)]
#[derive(Copy, Clone)]
#[repr(C)]
pub enum InstructionsKind {
    NOP = 0,

    PUSH,
    POP,
    DUP,

    ADD,
    SUB,
    MUL,
    DIV,

    JMP,
    JIF,

    EQ,
    GT,

    HALT,

    PRINT_DEBUG,
}

impl TryFrom<&str> for InstructionsKind {
    type Error = String;

    fn try_from(name: &str) -> Result<Self, Self::Error> {
        match name {
            "nop" => Ok(InstructionsKind::NOP),

            "push" => Ok(InstructionsKind::PUSH),
            "pop" => Ok(InstructionsKind::POP),
            "dup" => Ok(InstructionsKind::DUP),

            "add" => Ok(InstructionsKind::ADD),
            "mul" => Ok(InstructionsKind::MUL),
            "sub" => Ok(InstructionsKind::SUB),
            "div" => Ok(InstructionsKind::DIV),

            "jmp" => Ok(InstructionsKind::JMP),
            "jif" => Ok(InstructionsKind::JIF),

            "eq" => Ok(InstructionsKind::EQ),
            "gt" => Ok(InstructionsKind::GT),

            "halt" => Ok(InstructionsKind::HALT),

            "print_debug" => Ok(InstructionsKind::PRINT_DEBUG),

            _ => Err(format!("Unrecognised instruction '{}'.", name)),
        }
    }
}

#[derive(Copy, Clone)]
#[repr(C)]
pub struct Instructions {
    pub kind: InstructionsKind,
    pub value: Word,
}

pub struct InstBuilder(Instructions);

impl InstBuilder {
    #[inline(always)]
    pub fn new() -> Self {
        InstBuilder(unsafe { std::mem::zeroed() })
    }

    #[inline(always)]
    pub fn kind(&mut self, kind: InstructionsKind) -> &mut Self {
        self.0.kind = kind;
        self
    }

    #[inline(always)]
    pub fn operand(&mut self, value: Word) -> &mut Self {
        self.0.value = value;
        self
    }

    #[inline(always)]
    pub fn operand_u64(&mut self, value: u64) -> &mut Self {
        self.0.value.uint64 = value;
        self
    }

    #[inline(always)]
    pub fn operand_i64(&mut self, value: i64) -> &mut Self {
        self.0.value.int64 = value;
        self
    }

    #[inline(always)]
    pub fn operand_f64(&mut self, value: f64) -> &mut Self {
        self.0.value.float64 = value;
        self
    }

    #[inline(always)]
    pub fn operand_ptr(&mut self, value: usize) -> &mut Self {
        self.0.value.ptr = value;
        self
    }

    #[inline(always)]
    pub fn build(&self) -> Instructions {
        self.0
    }
}

impl Instructions {
    #[inline(always)]
    pub fn new() -> InstBuilder {
        InstBuilder::new()
    }

    #[inline(always)]
    pub fn empty() -> Self {
        unsafe { std::mem::zeroed() }
    }

    #[inline(always)]
    pub fn nop() -> Self {
        Self {
            kind: InstructionsKind::NOP,
            value: Word { uint64: 0 },
        }
    }

    #[inline(always)]
    pub fn push(value: u64) -> Self {
        Self {
            kind: InstructionsKind::PUSH,
            value: Word { uint64: value },
        }
    }

    #[inline(always)]
    pub fn pop() -> Self {
        Self {
            kind: InstructionsKind::NOP,
            value: Word { uint64: 0 },
        }
    }

    #[inline(always)]
    pub fn dup(value: u64) -> Self {
        Self {
            kind: InstructionsKind::DUP,
            value: Word { uint64: value },
        }
    }

    #[inline(always)]
    pub fn add() -> Self {
        Self {
            kind: InstructionsKind::ADD,
            value: Word { uint64: 0 },
        }
    }

    #[inline(always)]
    pub fn sub() -> Self {
        Self {
            kind: InstructionsKind::SUB,
            value: Word { uint64: 0 },
        }
    }

    #[inline(always)]
    pub fn mul() -> Self {
        Self {
            kind: InstructionsKind::MUL,
            value: Word { uint64: 0 },
        }
    }

    #[inline(always)]
    pub fn div() -> Self {
        Self {
            kind: InstructionsKind::DIV,
            value: Word { uint64: 0 },
        }
    }

    #[inline(always)]
    pub fn jmp(value: u64) -> Self {
        Self {
            kind: InstructionsKind::JMP,
            value: Word { uint64: value },
        }
    }

    #[inline(always)]
    pub fn jif(value: u64) -> Self {
        Self {
            kind: InstructionsKind::JIF,
            value: Word { uint64: value },
        }
    }

    #[inline(always)]
    pub fn eq() -> Self {
        Self {
            kind: InstructionsKind::EQ,
            value: Word { uint64: 0 },
        }
    }

    #[inline(always)]
    pub fn gt() -> Self {
        Self {
            kind: InstructionsKind::GT,
            value: Word { uint64: 0 },
        }
    }

    #[inline(always)]
    pub fn halt() -> Self {
        Self {
            kind: InstructionsKind::HALT,
            value: Word { uint64: 0 },
        }
    }

    #[inline(always)]
    pub fn print_debug() -> Self {
        Self {
            kind: InstructionsKind::PRINT_DEBUG,
            value: Word { uint64: 0 },
        }
    }
}

impl From<InstBuilder> for Instructions {
    fn from(builder: InstBuilder) -> Self {
        builder.build()
    }
}

impl From<InstructionsKind> for Instructions {
    fn from(kind: InstructionsKind) -> Self {
        Self {
            kind: kind,
            value: Word { uint64: 0 },
        }
    }
}

impl From<Instructions> for InstructionsKind {
    fn from(inst: Instructions) -> Self {
        inst.kind
    }
}

impl From<Instructions> for Word {
    fn from(inst: Instructions) -> Self {
        inst.value
    }
}

impl Display for Instructions {
    fn fmt(&self, f: &mut Formatter) -> std::fmt::Result {
        let print = match self.kind {
            InstructionsKind::NOP => format!("nop"),

            InstructionsKind::PUSH => format!("push {}", self.value.as_u64()),
            InstructionsKind::POP => format!("pop"),
            InstructionsKind::DUP => format!("dup {}", self.value.as_u64()),

            InstructionsKind::ADD => format!("add"),
            InstructionsKind::MUL => format!("mul"),
            InstructionsKind::SUB => format!("sub"),
            InstructionsKind::DIV => format!("div"),

            InstructionsKind::JMP => format!("jmp {}", self.value.as_u64()),
            InstructionsKind::JIF => format!("jif {}", self.value.as_u64()),

            InstructionsKind::EQ => format!("eq"),
            InstructionsKind::GT => format!("gt"),

            InstructionsKind::HALT => format!("halt"),

            InstructionsKind::PRINT_DEBUG => format!("print_debug"),
        };

        f.pad_integral(true, "", &print)
    }
}
