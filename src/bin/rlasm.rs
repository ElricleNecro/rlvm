extern crate rlvm;

use std::collections::HashMap;
use std::convert::TryFrom;
use std::fs::File;
use std::io::{self, BufReader, Read};
use std::path::Path;

use rlvm::{Instructions, InstructionsKind, Rlvm};

const COMMENT_DELIM: char = '#';

struct Unresolved {
    location: usize,
    name: String,
}

struct Records {
    labels: HashMap<String, u64>,
    unresolved: Vec<Unresolved>,
}

impl Records {
    fn new() -> Self {
        Self {
            labels: HashMap::new(),
            unresolved: Vec::new(),
        }
    }

    fn add_label(&mut self, name: &str, addr: u64) {
        if self.labels.contains_key(name) {
            return;
        }

        self.labels.insert(name.to_string(), addr);
    }

    fn add_unresolved(&mut self, location: usize, name: &str) {
        self.unresolved.push(Unresolved {
            location: location,
            name: name.to_string(),
        });
    }

    fn resolve(&self, program: &mut Vec<Instructions>) {
        for jmp in &self.unresolved {
            program[jmp.location].value.uint64 = self.labels[&jmp.name];
        }
    }
}

fn load_file<P: AsRef<Path>>(path: P) -> io::Result<String> {
    let mut code = String::new();
    let mut reader = BufReader::new(File::open(path)?);

    reader.read_to_string(&mut code)?;

    Ok(code)
}

fn parse<P: AsRef<Path>>(path: P) -> Result<Rlvm, String> {
    let mut vm = Rlvm::new();
    let mut records = Records::new();
    let code = load_file(path).map_err(|e| format!("{:?}", e))?;

    for line in code.lines().filter(|s| s.len() != 0) {
        let line = line.trim();
        if line.starts_with(COMMENT_DELIM) {
            continue;
        }

        let line: Vec<&str> = line.split_whitespace().filter(|s| s.len() != 0).collect();
        let mut idx = 0;
        while idx < line.len() && !line[idx].starts_with(COMMENT_DELIM) {
            if line[idx].ends_with(':') {
                records.add_label(&line[idx].replace(':', ""), vm.program.len() as u64);
            } else {
                let inst_kind = InstructionsKind::try_from(line[idx])?;
                let mut inst: Instructions = inst_kind.into();

                vm.push_instructions(match inst_kind {
                    InstructionsKind::PUSH => inst,
                    InstructionsKind::DUP | InstructionsKind::JMP | InstructionsKind::JIF => {
                        idx += 1;
                        inst.value.uint64 = line[idx].parse::<u64>().map_err(|e| format!("{:?}", e))?;
                        inst
                    },
                    _ => inst,
                });
            }

            idx += 1;
        }
    }

    records.resolve(&mut vm.program);

    Ok(vm)
}

pub fn main() -> Result<(), String> {
    let args: Vec<String> = std::env::args().collect();
    if args.len() != 3 {
        return Err(format!("Usage: {} [program.rlasm] [output.rl]", args[0]));
    }

    parse(&args[1])?
        .write(&args[2])
        .map_err(|e| format!("{:?}", e))?;

    Ok(())
}
