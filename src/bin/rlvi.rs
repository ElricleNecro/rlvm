extern crate rlvm;

use rlvm::Rlvm;

fn main() -> Result<(), String> {
    let args: Vec<String> = std::env::args().collect();
    if args.len() != 2 {
        return Err(format!("Usage: {} [program.rl]", args[0]));
    }

    let mut vm = Rlvm::from_file(&args[1]).map_err(|err| format!("{:?}", err))?;
    vm.run(69).map_err(|err| format!("{:?}", err))?;
    vm.show_stack();

    Ok(())
}
