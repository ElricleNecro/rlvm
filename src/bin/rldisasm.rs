extern crate rlvm;

use rlvm::Rlvm;

pub fn main() -> Result<(), String> {
    let args: Vec<String> = std::env::args().collect();
    if args.len() != 2 {
        return Err(format!("Usage: {} [program.rl]", args[0]));
    }

    let vm = Rlvm::from_file(&args[1]).map_err(|err| format!("{:?}", err))?;

    for inst in vm.program {
        println!("{}", inst);
    }

    Ok(())
}
