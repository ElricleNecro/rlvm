use std::vec::Vec;

mod instructions;
pub use instructions::Instructions;
pub use instructions::InstructionsKind;

mod error;
pub use error::Error;

use std::fs::{self, File};
use std::io::{BufReader, BufWriter, Read, Result as ResultIo, Write};
use std::path::Path;

const RVLM_STACK_CAPACITY: usize = 1024;

#[repr(C)]
#[derive(Copy, Clone)]
pub union Word {
    pub uint64: u64,
    pub int64: i64,
    pub float64: f64,
    pub ptr: usize,
}

impl Word {
    #[inline(always)]
    pub fn as_u64(&self) -> u64 {
        unsafe { self.uint64 }
    }

    #[inline(always)]
    pub fn as_i64(&self) -> i64 {
        unsafe { self.int64 }
    }

    #[inline(always)]
    pub fn as_f64(&self) -> f64 {
        unsafe { self.float64 }
    }

    #[inline(always)]
    pub fn as_ptr(&self) -> usize {
        unsafe { self.ptr }
    }
}

pub struct Rlvm {
    stack: [Word; RVLM_STACK_CAPACITY],

    pub program: Vec<Instructions>,

    ip: usize,
    sp: usize,

    halt: bool,
}

impl Rlvm {
    #[inline(always)]
    pub fn new() -> Self {
        Self {
            stack: [Word { uint64: 0 }; RVLM_STACK_CAPACITY],

            program: Vec::new(),

            ip: 0,
            sp: 0,

            halt: false,
        }
    }

    #[inline]
    pub fn from_memory(program: &Vec<Instructions>) -> Self {
        let mut vm = Self::new();

        vm.program.extend(program);

        vm
    }

    pub fn from_file<P: AsRef<Path>>(path: P) -> ResultIo<Self> {
        let mut vm = Rlvm::new();

        let path = path.as_ref();
        let fsize = fs::metadata(path)?.len() as usize;
        let dsize = std::mem::size_of::<Instructions>();
        let array_size = fsize / dsize;

        if fsize % dsize != 0 {
            return Err(std::io::Error::new(
                std::io::ErrorKind::Other,
                format!(
                    "File size is not valid ({} is not a multiple of {}).",
                    fsize, dsize
                ),
            ));
        }

        let mut freader = BufReader::new(File::open(path)?);
        vm.program = Vec::with_capacity(array_size);

        unsafe {
            let buffer = std::slice::from_raw_parts_mut(vm.program.as_mut_ptr() as *mut u8, fsize);
            freader.read_exact(buffer)?;
            vm.program.set_len(array_size);
        }

        Ok(vm)
    }

    #[inline(always)]
    pub fn push_instructions(&mut self, inst: Instructions) {
        self.program.push(inst);
    }

    pub fn show_stack(&self) {
        println!("Stack:");

        if self.sp == 0 {
            println!("[empty]");
            return;
        }

        for idx in 0..self.sp {
            println!("\t{}", unsafe { self.stack[idx].uint64 });
        }
    }

    pub fn execute(&mut self) -> Result<(), Error> {
        if self.ip >= self.program.len() {
            return Err(Error::ILLEGAL_INSTRUCTION_ACCESS);
        }

        let current = self.program[self.ip];
        match current.kind {
            InstructionsKind::NOP => self.ip += 1,

            InstructionsKind::PUSH => {
                if self.sp >= self.stack.len() {
                    return Err(Error::STACK_OVERFLOW);
                }

                self.stack[self.sp] = current.value;
                self.sp += 1;
                self.ip += 1;
            }
            InstructionsKind::POP => {
                if self.sp <= 0 {
                    return Err(Error::STACK_UNDERFLOW);
                }

                self.sp -= 1;
                self.ip += 1;
            }
            InstructionsKind::DUP => {
                let operand = current.value.as_u64() as usize;

                if self.sp - operand <= 0 {
                    return Err(Error::STACK_UNDERFLOW);
                }
                if self.sp >= self.stack.len() {
                    return Err(Error::STACK_OVERFLOW);
                }

                self.stack[self.sp] = self.stack[self.sp - 1 - operand];
                self.sp += 1;
                self.ip += 1;
            }

            InstructionsKind::ADD => {
                if self.sp < 2 {
                    return Err(Error::STACK_UNDERFLOW);
                }

                self.stack[self.sp - 2].uint64 =
                    self.stack[self.sp - 2].as_u64() + self.stack[self.sp - 1].as_u64();
                self.sp -= 1;
                self.ip += 1;
            }
            InstructionsKind::SUB => {
                if self.sp < 2 {
                    return Err(Error::STACK_UNDERFLOW);
                }

                self.stack[self.sp - 2].uint64 =
                    self.stack[self.sp - 2].as_u64() - self.stack[self.sp - 1].as_u64();
                self.sp -= 1;
                self.ip += 1;
            }
            InstructionsKind::MUL => {
                if self.sp < 2 {
                    return Err(Error::STACK_UNDERFLOW);
                }

                self.stack[self.sp - 2].uint64 =
                    self.stack[self.sp - 2].as_u64() * self.stack[self.sp - 1].as_u64();
                self.sp -= 1;
                self.ip += 1;
            }
            InstructionsKind::DIV => {
                if self.sp < 2 {
                    return Err(Error::STACK_UNDERFLOW);
                }

                self.stack[self.sp - 2].uint64 =
                    self.stack[self.sp - 2].as_u64() / self.stack[self.sp - 1].as_u64();
                self.sp -= 1;
                self.ip += 1;
            }

            InstructionsKind::JMP => self.ip = current.value.as_u64() as usize,
            InstructionsKind::JIF => {
                if self.sp < 1 {
                    return Err(Error::STACK_UNDERFLOW);
                }

                if self.stack[self.sp - 1].as_u64() >= 1 {
                    self.ip = current.value.as_u64() as usize;
                    self.sp -= 1;
                } else {
                    self.ip += 1;
                }
            }

            InstructionsKind::EQ => {
                if self.sp < 2 {
                    return Err(Error::STACK_UNDERFLOW);
                }

                if self.stack[self.sp - 2].as_u64() == self.stack[self.sp - 1].as_u64() {
                    self.stack[self.sp - 2].uint64 = 1;
                } else {
                    self.stack[self.sp - 2].uint64 = 0;
                }

                self.sp -= 1;
                self.ip += 1;
            }
            InstructionsKind::GT => {
                if self.sp < 2 {
                    return Err(Error::STACK_UNDERFLOW);
                }

                if self.stack[self.sp - 2].as_u64() > self.stack[self.sp - 1].as_u64() {
                    self.stack[self.sp - 2].uint64 = 1;
                } else {
                    self.stack[self.sp - 2].uint64 = 0;
                }

                self.sp -= 1;
                self.ip += 1;
            }

            InstructionsKind::HALT => {
                self.halt = true;
                self.ip += 1;
            }

            InstructionsKind::PRINT_DEBUG => {
                if self.sp < 1 {
                    return Err(Error::STACK_UNDERFLOW);
                }

                println!("{}", self.stack[self.sp - 1].as_u64());

                self.ip += 1;
            }
        };

        Ok(())
    }

    pub fn run(&mut self, limit: usize) -> Result<(), Error> {
        let mut limit = limit;

        while limit != 0 && !self.halt {
            match self.execute() {
                Ok(()) => {}
                Err(e) => {
                    return Err(e);
                }
            };

            if limit > 0 {
                limit -= 1;
            }
        }

        Ok(())
    }

    pub fn write<P: AsRef<Path>>(&self, path: P) -> ResultIo<()> {
        let path = path.as_ref();

        let mut freader = BufWriter::new(File::create(path)?);
        let fsize = self.program.len() * std::mem::size_of::<Instructions>();

        unsafe {
            let buffer = std::slice::from_raw_parts(self.program.as_ptr() as *const u8, fsize);
            freader.write_all(buffer)?;
        }

        Ok(())
    }
}
