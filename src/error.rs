#[allow(non_camel_case_types)]
pub enum Error {
    STACK_OVERFLOW,
    STACK_UNDERFLOW,

    ILLEGAL_INSTRUCTION,
    ILLEGAL_INSTRUCTION_ACCESS,
    ILLEGAL_OPERAND,

    DIV_BY_ZERO,
}

impl std::fmt::Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        let print = match self {
            Error::STACK_OVERFLOW => format!("STACK_OVERFLOW"),
            Error::STACK_UNDERFLOW => format!("STACK_UNDERFLOW"),

            Error::ILLEGAL_INSTRUCTION => format!("ILLEGAL_INSTRUCTION"),
            Error::ILLEGAL_INSTRUCTION_ACCESS => format!("ILLEGAL_INSTRUCTION_ACCESS"),
            Error::ILLEGAL_OPERAND => format!("ILLEGAL_OPERAND"),

            Error::DIV_BY_ZERO => format!("DIV_BY_ZERO"),
        };

        f.pad_integral(true, "", &print)
    }
}

impl std::fmt::Debug for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        let print = match self {
            Error::STACK_OVERFLOW => format!("STACK_OVERFLOW"),
            Error::STACK_UNDERFLOW => format!("STACK_UNDERFLOW"),

            Error::ILLEGAL_INSTRUCTION => format!("ILLEGAL_INSTRUCTION"),
            Error::ILLEGAL_INSTRUCTION_ACCESS => format!("ILLEGAL_INSTRUCTION_ACCESS"),
            Error::ILLEGAL_OPERAND => format!("ILLEGAL_OPERAND"),

            Error::DIV_BY_ZERO => format!("DIV_BY_ZERO"),
        };

        f.pad_integral(true, "", &print)
    }
}
